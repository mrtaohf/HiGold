package com.doraemnn.higold.app;

/**
 * 描述：常量名
 * <p>
 * Created by WK on 2017/3/1
 */
public class Constants {

    //第一次是否进入引导页
    public static final String GUIDE_IS_OPEN = "guide_is_open";

    // 首页Tab中传递的参数
    public static final String BUNDLE_HOME_POSITION = "bundle_home_position";

    // 分页大小
    public static final int PAGE_SIZE = 20;

    // 跳转到详情页传递参数用到的Key
    public static final String BUNDLE_DETAIL_URL = "bundle_detail_url";
    public static final String BUNDLE_DETAIL_TITLE = "bundle_detail_title";
    public static final String BUNDLE_DETAIL_IMAGE = "bundle_detail_image";
}
