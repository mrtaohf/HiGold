package com.doraemnn.higold.app;

import android.app.Application;
import android.content.Context;

import com.doraemnn.higold.imageload.GlideLimitInterceptor;
import com.doraemnn.higold.model.http.HeaderInterceptor;
import com.doraemnn.higold.model.http.LoggerInterceptor;
import com.doraemnn.higold.model.http.OkHttpClientApi;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;

/**
 * 描述：
 * <p>
 * Created by WK on 2017/3/1
 */
public class HiGoldApp extends Application {

    private static HiGoldApp mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        //初始化 LeakCanary  检测内存泄漏
//        LeakCanary.install(this);

        initAll();
    }

    private void initAll() {

        initOkHttp();

        initGlide();
    }

    private void initGlide() {
        GlideLimitInterceptor glideLimitInterceptor = new GlideLimitInterceptor();
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(5000, TimeUnit.MILLISECONDS)
                .readTimeout(5000, TimeUnit.MILLISECONDS)
                .addInterceptor(glideLimitInterceptor)
                .build();
        OkHttpClientApi.getInstance(okHttpClient);

    }

    private void initOkHttp() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(5000, TimeUnit.MILLISECONDS)
                .readTimeout(5000, TimeUnit.MILLISECONDS)
                .addInterceptor(new HeaderInterceptor())
                .addInterceptor(new LoggerInterceptor(LoggerInterceptor.Level.HEADERS))
                .cache(new Cache(new File(getCacheDir(), "gold-http"), 10 * 1024 * 1024))
                .build();
        OkHttpClientApi.getInstance(okHttpClient);
    }

    public static Context getInstance() {
        return mInstance;
    }
}
