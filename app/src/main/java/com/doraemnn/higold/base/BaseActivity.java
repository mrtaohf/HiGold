package com.doraemnn.higold.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.doraemnn.higold.R;
import com.doraemnn.higold.utils.AppManager;
import com.doraemnn.higold.utils.ClazzFormatUtil;
import com.orhanobut.logger.Logger;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseActivity<T extends BasePresenter,E extends BaseModel> extends AppCompatActivity {

    public T mPresenter;
    public E mModel;
    public Context mContext;
    private Unbinder mBind;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutResId());
        mBind = ButterKnife.bind(this);
        preInit(savedInstanceState);
        mContext = this;
        AppManager.getInstance().addActivity(this);
        initMVP();
        findViews();
        initViews();
        initComplete();
    }

    private void initMVP() {
        mPresenter = ClazzFormatUtil.getT(this,0);
        mModel = ClazzFormatUtil.getT(this,1);
        if (mPresenter != null) {
            mPresenter.mContext = this;
        }
        initPresenter();
    }

    protected abstract void initPresenter();


    protected abstract void preInit(Bundle savedInstanceState);

    protected abstract int layoutResId();

    /**
     * findViewById
     */
    protected abstract void findViews();

    /**
     * view初始化
     */
    protected abstract void initViews();

    /**
     * 初始化完成
     */
    protected abstract void initComplete();

    /**
     * 设置toolbar标题
     */

    protected void setUniversalToolbar(CharSequence title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(title);
        }
    }


    /**
     * 网络不可用
     */
    protected void netWorkUnable(Throwable e) {
        Logger.e(e, "netWorkUnable");
        toast(R.string.network_unable);
    }

    /**
     * Toast统一显示入口
     */
    protected void toast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    /**
     * Toast统一显示入口
     */
    protected void toast(@StringRes int id) {
        Toast.makeText(getApplicationContext(), id, Toast.LENGTH_SHORT).show();
    }

    /**
     * 隐藏软键盘
     */
    protected void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) mPresenter.unSubscribe();
        if (mBind != null) mBind.unbind();
        AppManager.getInstance().finishActivity(this);
    }
}
