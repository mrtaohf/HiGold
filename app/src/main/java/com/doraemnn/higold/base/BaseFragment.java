package com.doraemnn.higold.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.doraemnn.higold.utils.ClazzFormatUtil;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 描述：
 * <p>
 * Created by WK on 2017/3/1
 */
public abstract class BaseFragment<T extends BasePresenter,E extends BaseModel> extends Fragment  {

    private View mContentView;
    public T mPresenter;
    public E mModel;

    protected boolean mVisible = true;
    private boolean mPrepared;
    private Unbinder mUnbinder;
    protected Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (mContentView == null) {
            mContentView = inflater.inflate(layoutResId(), container, false);
            mUnbinder = ButterKnife.bind(this, mContentView);
            preInit(savedInstanceState);
            initMVP();
            findViews(mContentView);
            initViews();
            mPrepared = true;
            lazyLoad();
        }
        return mContentView;

    }

    private void initMVP() {
        mPresenter = ClazzFormatUtil.getT(this,0);
        mModel = ClazzFormatUtil.getT(this,1);
        if (mPresenter != null) {
            mPresenter.mContext = this.getActivity();
        }
        initPresenter();
    }

    protected abstract void initPresenter();

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            mVisible = true;
            onVisible();
        } else {
            mVisible = false;
            onInvisible();
        }
    }

    /**
     * view初始化
     */
    protected abstract void initViews();

    /**
     * 布局id
     */
    @LayoutRes
    protected abstract int layoutResId();


    /**
     * 在{@link #findViews(View)}💰执行
     *
     * @param savedInstanceState {@link Bundle}
     */
    protected void preInit(Bundle savedInstanceState) {

    }

    /**
     * 放置findView方法
     */
    protected abstract void findViews(View view);


    /**
     * {@link android.support.v4.app.Fragment}可见
     */
    protected void onVisible() {
        lazyLoad();
    }

    /**
     * {@link android.support.v4.app.Fragment}不可见
     */
    protected void onInvisible() {

    }

    private void lazyLoad() {
        if (!mPrepared || !mVisible) {
            return;
        }
        loadData();
    }

    /**
     * 初始化完毕且页面可见
     */
    protected abstract void loadData();

    /**
     * Toast统一显示入口
     */
    protected void toast(@StringRes int id) {
        Toast.makeText(getActivity().getApplicationContext(), id, Toast.LENGTH_SHORT).show();
    }

    /**
     * Toast统一显示入口
     */
    protected void toast(String text) {
        Toast.makeText(getActivity().getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mContentView = null;
        }
    }
}
