package com.doraemnn.higold.base;

import android.content.Context;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * 描述：
 * <p>
 * Created by WK on 2017/3/1
 */
public abstract class BasePresenter<V,M>{

    private CompositeSubscription mCompositeSubscription = new CompositeSubscription();

    public Context mContext;
    public V mView;
    public M mModel;

    public void setVM(V v,M m) {
        this.mView = v;
        this.mModel = m;
        this.onStart();
    }

    public void onStart() {}


    /**
     * 防止内存泄漏
     * @param subscription
     */
    protected void addSubscribe(Subscription subscription) {
        mCompositeSubscription.add(subscription);
    }

    public void unSubscribe() {
        mCompositeSubscription.clear();
    }
}
