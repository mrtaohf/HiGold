package com.doraemnn.higold.base;

import com.doraemnn.higold.model.http.exception.ApiException;

/**
 * 描述：
 * <p>
 * Created by WK on 2017/3/1
 */
public interface IBaseView {

    /**
     * 显示加载数据View
     */
    void showLoading();

    /**
     * 网络失败后，显示的错误提示
     */
    void showErrorView(ApiException e);

    /**
     * 隐藏加载View
     */
    void hideLoading();

}
