package com.doraemnn.higold.common;

import com.doraemnn.higold.base.BaseFragment;

import java.util.HashMap;
import java.util.Map;

/**
 * 主页面的Fragment工厂
 * Created by hongfei.tao on 2017/3/17 20:00.
 */
public class TabFragmentManager {

    private Map<String, BaseFragment> mFragmentContainer;

    private static TabFragmentManager sInstance;

    private TabFragmentManager() {
        mFragmentContainer = new HashMap<>();
    }

    public static TabFragmentManager getInstance() {
        if (sInstance == null) {
            synchronized (TabFragmentManager.class) {
                if (sInstance == null) {
                    sInstance = new TabFragmentManager();
                }
            }
        }

        return sInstance;
    }

    public BaseFragment getFragment(Class<? extends BaseFragment> clazz) {
        BaseFragment fragment = mFragmentContainer.get(clazz.getSimpleName());

        if (fragment == null) {
            try {
                fragment = clazz.newInstance();
                mFragmentContainer.put(clazz.getSimpleName(), fragment);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return fragment;
    }

}
