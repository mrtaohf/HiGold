package com.doraemnn.higold.imageload;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.doraemnn.higold.imageload.integration.OkHttpGlideModule;
import com.doraemnn.higold.imageload.integration.OkHttpUrlLoader;
import com.doraemnn.higold.model.http.OkHttpClientApi;

import java.io.InputStream;

/**
 * Created by guilin on 16/7/11.
 * <p>
 * 描述:替换Glide's default{@link com.bumptech.glide.load.model.ModelLoader}
 */

public class AppGlideModule extends OkHttpGlideModule {

    @Override
    public void registerComponents(Context context, Glide glide) {
        glide.register(GlideUrl.class,
                InputStream.class,
                new OkHttpUrlLoader.Factory(OkHttpClientApi.getOkHttpClient()));
    }
}