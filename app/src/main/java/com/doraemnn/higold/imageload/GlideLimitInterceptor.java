package com.doraemnn.higold.imageload;

/**
 * Created by guilin on 16/7/11.
 * <p>
 * 描述:图片加载限制拦截器,例如:图片仅在wifi下加载
 */

public final class GlideLimitInterceptor extends GlideInterceptor {
    /**
     * 
     * @return true 是取消请求图片 
     */
    @Override
    protected boolean cancelRequest() {
        return true;
    }
}
