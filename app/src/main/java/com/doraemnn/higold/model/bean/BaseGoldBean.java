package com.doraemnn.higold.model.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by junwang on 2017/3/19.
 */

public class BaseGoldBean<T> implements Serializable{

    private int code;
    private String error;

    @SerializedName("results")
    private T results;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public T getResults() {
        return results;
    }

    public void setResults(T result) {
        this.results = result;
    }
}
