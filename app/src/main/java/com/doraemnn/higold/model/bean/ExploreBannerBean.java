package com.doraemnn.higold.model.bean;

/**
 * 当前类注释：发现页面---banner
 * <p>
 * Author :LeonWang <p>
 * Created  2017/3/31.17:46 <p>
 * Description:
 * <p>
 * E-mail:lijiawangjun@gmail.com
 */

public class ExploreBannerBean {


    /**
     * description : 我们是无花果大冒险（北京）科技有限公司，是一家成立 2 年，在国内不太有存在感的初创企业。 我们开发并运营着欧美地区用户最多的手游直播社区 Shou，今年初开始同时开发一款面向国内市场的视频群聊社交 App 碰碰，抄袭的美国比较火的产品 Houseparty。 我们团队现在有不…
     * startedAt : {"__type":"Date","iso":"2017-03-28T16:00:00.000Z"}
     * updatedAt : 2017-03-29T06:39:15.606Z
     * objectId : 58db3964a22b9d00647ae964
     * createdAt : 2017-03-29T04:34:44.556Z
     * type : entry
     * title : [北京望京] [15K-35K] 无花果寻找合适的队友一起大冒险
     * relatedObjectId : 58da723a5c497d00570d34cc
     * url : https://juejin.im/post/58da59afb123db199f4f74cf
     * position : hero
     * screenshot : {"mime_type":"image/png","updatedAt":"2017-03-29T04:34:44.028Z","key":"7d18b56d4bf88a37074a.png","name":"480x400.png","objectId":"58db3964b123db199f53ef1f","createdAt":"2017-03-29T04:34:44.028Z","__type":"File","url":"https://dn-mhke0kuv.qbox.me/7d18b56d4bf88a37074a.png","metaData":{"owner":"551ca9d6e4b0cd5b6237420e","size":33190,"mime_type":"image/png"},"bucket":"mhke0kuv"}
     * platform : web
     * endedAt : {"__type":"Date","iso":"2017-04-02T16:00:00.000Z"}
     */

    private String description;
    private StartedAtEntity startedAt;
    private String updatedAt;
    private String objectId;
    private String createdAt;
    private String type;
    private String title;
    private String relatedObjectId;
    private String url;
    private String position;
    private ScreenshotEntity screenshot;
    private String platform;
    private EndedAtEntity endedAt;

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStartedAt(StartedAtEntity startedAt) {
        this.startedAt = startedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setRelatedObjectId(String relatedObjectId) {
        this.relatedObjectId = relatedObjectId;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setScreenshot(ScreenshotEntity screenshot) {
        this.screenshot = screenshot;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public void setEndedAt(EndedAtEntity endedAt) {
        this.endedAt = endedAt;
    }

    public String getDescription() {
        return description;
    }

    public StartedAtEntity getStartedAt() {
        return startedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getObjectId() {
        return objectId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getRelatedObjectId() {
        return relatedObjectId;
    }

    public String getUrl() {
        return url;
    }

    public String getPosition() {
        return position;
    }

    public ScreenshotEntity getScreenshot() {
        return screenshot;
    }

    public String getPlatform() {
        return platform;
    }

    public EndedAtEntity getEndedAt() {
        return endedAt;
    }

    public static class StartedAtEntity {
        /**
         * __type : Date
         * iso : 2017-03-28T16:00:00.000Z
         */

        private String __type;
        private String iso;

        public void set__type(String __type) {
            this.__type = __type;
        }

        public void setIso(String iso) {
            this.iso = iso;
        }

        public String get__type() {
            return __type;
        }

        public String getIso() {
            return iso;
        }
    }

    public static class ScreenshotEntity {
        /**
         * mime_type : image/png
         * updatedAt : 2017-03-29T04:34:44.028Z
         * key : 7d18b56d4bf88a37074a.png
         * name : 480x400.png
         * objectId : 58db3964b123db199f53ef1f
         * createdAt : 2017-03-29T04:34:44.028Z
         * __type : File
         * url : https://dn-mhke0kuv.qbox.me/7d18b56d4bf88a37074a.png
         * metaData : {"owner":"551ca9d6e4b0cd5b6237420e","size":33190,"mime_type":"image/png"}
         * bucket : mhke0kuv
         */

        private String mime_type;
        private String updatedAt;
        private String key;
        private String name;
        private String objectId;
        private String createdAt;
        private String __type;
        private String url;
        private MetaDataEntity metaData;
        private String bucket;

        public void setMime_type(String mime_type) {
            this.mime_type = mime_type;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setObjectId(String objectId) {
            this.objectId = objectId;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public void set__type(String __type) {
            this.__type = __type;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public void setMetaData(MetaDataEntity metaData) {
            this.metaData = metaData;
        }

        public void setBucket(String bucket) {
            this.bucket = bucket;
        }

        public String getMime_type() {
            return mime_type;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public String getKey() {
            return key;
        }

        public String getName() {
            return name;
        }

        public String getObjectId() {
            return objectId;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public String get__type() {
            return __type;
        }

        public String getUrl() {
            return url;
        }

        public MetaDataEntity getMetaData() {
            return metaData;
        }

        public String getBucket() {
            return bucket;
        }

        public static class MetaDataEntity {
            /**
             * owner : 551ca9d6e4b0cd5b6237420e
             * size : 33190
             * mime_type : image/png
             */

            private String owner;
            private int size;
            private String mime_type;

            public void setOwner(String owner) {
                this.owner = owner;
            }

            public void setSize(int size) {
                this.size = size;
            }

            public void setMime_type(String mime_type) {
                this.mime_type = mime_type;
            }

            public String getOwner() {
                return owner;
            }

            public int getSize() {
                return size;
            }

            public String getMime_type() {
                return mime_type;
            }
        }
    }

    public static class EndedAtEntity {
        /**
         * __type : Date
         * iso : 2017-04-02T16:00:00.000Z
         */

        private String __type;
        private String iso;

        public void set__type(String __type) {
            this.__type = __type;
        }

        public void setIso(String iso) {
            this.iso = iso;
        }

        public String get__type() {
            return __type;
        }

        public String getIso() {
            return iso;
        }
    }
}
