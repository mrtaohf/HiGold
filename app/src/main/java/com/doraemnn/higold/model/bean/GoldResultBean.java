package com.doraemnn.higold.model.bean;

import java.util.List;

/**
 * 首页单个条目数据Bean
 * Created by hongfei.tao on 2017/3/18.
 */
public class GoldResultBean {

    /*private List<ResultsBean> results;

    private String code;

    private String error;

    public List<ResultsBean> getResults() {
        return results;
    }

    public void setResults(List<ResultsBean> results) {
        this.results = results;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public static class ResultsBean {*/
        /**
         * tagsTitleArray : ["Android"]
         * category : android
         * tags : {"__type":"Relation","className":"Tag"}
         * hotIndex : 1502
         * updatedAt : 2017-03-18T12:12:04.853Z
         * viewsCount : 970
         * subscribers : {"__type":"Relation","className":"_User"}
         * collectionCount : 53
         * content : Android 最近推出一个新的基于物理学的动画支持库，命名为：SpringAnimation（弹簧动画），发布在 Support Library 25.3.0 里面。昨天，Google Android 研发工程师「Nick Butcher」在 Twitter 上发布推文予以公…
         * objectId : 58ccd58c5c497d0057bda6ea
         * hot : false
         * original : true
         * createdAt : 2017-03-18T06:37:00.314Z
         * type : post
         * title : Android 新推出基于物理的动画库，完全诠释什么叫做弹簧效果
         * english : false
         * rankIndex : 39.129381362201585
         * url : https://juejin.im/entry/58ccd58c5c497d0057bda6ea/view
         * entryView : {"__type":"Pointer","className":"EntryView","objectId":"58ccd58cac502e005894f69b"}
         * gfw : false
         * commentsCount : 0
         * user : {"viewedEntriesCount":829,"role":"editor","totalCollectionsCount":9416,"allowNotification":false,"subscribedTagsCount":2,"appliedEditorAt":{"__type":"Date","iso":"2016-08-04T14:37:09.529Z"},"followersCount":2646,"updatedAt":"2017-03-18T12:12:04.837Z","postedEntriesCount":25,"latestCollectionUserNotification":{"__type":"Date","iso":"2017-03-18T06:19:42.723Z"},"commentedEntriesCount":79,"weeklyEmail":0,"collectedEntriesCount":20,"objectId":"57520855a341310063bd731f","postedPostsCount":21,"isAuthor":true,"username":"亦枫","latestLoginedInAt":{"__type":"Date","iso":"2017-03-18T11:09:32.550Z"},"createdAt":"2016-06-03T22:44:37.465Z","totalHotIndex":200744,"blogAddress":"http://yifeng.studio/","self_description":"个人微信公众号：安卓笔记侠（微信号：NiaoTech），博客地址：http://yifeng.studio/","latestCheckedNotificationAt":{"__type":"Date","iso":"2016-10-19T01:26:35.226Z"},"className":"_User","emailVerified":false,"totalCommentsCount":251,"installation":{"__type":"Pointer","className":"_Installation","objectId":"1BwPTNoShoYBhrdjtgvUtKwWu6fMxQSy"},"blacklist":false,"mobilePhoneNumber":"18801772064","__type":"Pointer","apply":false,"followeesCount":16,"deviceType":"android","avatar_hd":"https://user-gold-cdn.xitu.io/2016/11/29/d88cabd86b9ab6c478121f9e15fb55a2","editorType":"md","jobTitle":"AndroidDeveloper","company":"微信公号：安卓笔记侠","authData":{"weibo":{"uid":"3137408085","expiration_in":"36000","access_token":"2.00NKP17DWKIDWD9dea41a965F1LzzC"},"github":{"access_token":"cfba2cc9b08809aaf8d9e952a2964f7f5faa24d8","expiration_in":"36000","uid":"8175587","html_url":"https://github.com/Mike-bel","bio":null,"username":"Mike-bel","nickname":"YiFeng","avatar_url":"https://avatars.githubusercontent.com/u/8175587?v=3"}},"avatar_large":"https://dn-mhke0kuv.qbox.me/Bink3GWRzTiAzjGITIV1VHwGczJbr466RDX0lfI3","mobilePhoneVerified":true}
         * subscribersCount : 0
         * screenshot : {"mime_type":"image/jpeg","updatedAt":"2017-03-18T06:36:41.028Z","key":"03973e3eb1349923eb92.jpg","name":"timg.jpg","objectId":"58ccd57961ff4b006025d87f","createdAt":"2017-03-18T06:36:41.028Z","__type":"File","url":"https://dn-mhke0kuv.qbox.me/03973e3eb1349923eb92.jpg","metaData":{"owner":"57520855a341310063bd731f","size":379258,"mime_type":"image/jpeg"},"bucket":"mhke0kuv"}
         * originalUrl : https://juejin.im/post/58ccd4fca22b9d00642849bf
         * latestCommentAt : {"__type":"Date","iso":"2017-03-18T09:04:04.495Z"}
         * userSubmit : true
         */

        private String category;
        private TagsBean tags;
        private int hotIndex;
        private String updatedAt;
        private int viewsCount;
        private SubscribersBean subscribers;
        private int collectionCount;
        private String content;
        private String objectId;
        private boolean hot;
        private boolean original;
        private String createdAt;
        private String type;
        private String title;
        private boolean english;
        private double rankIndex;
        private String url;
        private EntryViewBean entryView;
        private boolean gfw;
        private int commentsCount;
        private UserBean user;
        private int subscribersCount;
        private ScreenshotBean screenshot;
        private String originalUrl;
        private LatestCommentAtBean latestCommentAt;
        private boolean userSubmit;
        private List<String> tagsTitleArray;

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public TagsBean getTags() {
            return tags;
        }

        public void setTags(TagsBean tags) {
            this.tags = tags;
        }

        public int getHotIndex() {
            return hotIndex;
        }

        public void setHotIndex(int hotIndex) {
            this.hotIndex = hotIndex;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public int getViewsCount() {
            return viewsCount;
        }

        public void setViewsCount(int viewsCount) {
            this.viewsCount = viewsCount;
        }

        public SubscribersBean getSubscribers() {
            return subscribers;
        }

        public void setSubscribers(SubscribersBean subscribers) {
            this.subscribers = subscribers;
        }

        public int getCollectionCount() {
            return collectionCount;
        }

        public void setCollectionCount(int collectionCount) {
            this.collectionCount = collectionCount;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getObjectId() {
            return objectId;
        }

        public void setObjectId(String objectId) {
            this.objectId = objectId;
        }

        public boolean isHot() {
            return hot;
        }

        public void setHot(boolean hot) {
            this.hot = hot;
        }

        public boolean isOriginal() {
            return original;
        }

        public void setOriginal(boolean original) {
            this.original = original;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public boolean isEnglish() {
            return english;
        }

        public void setEnglish(boolean english) {
            this.english = english;
        }

        public double getRankIndex() {
            return rankIndex;
        }

        public void setRankIndex(double rankIndex) {
            this.rankIndex = rankIndex;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public EntryViewBean getEntryView() {
            return entryView;
        }

        public void setEntryView(EntryViewBean entryView) {
            this.entryView = entryView;
        }

        public boolean isGfw() {
            return gfw;
        }

        public void setGfw(boolean gfw) {
            this.gfw = gfw;
        }

        public int getCommentsCount() {
            return commentsCount;
        }

        public void setCommentsCount(int commentsCount) {
            this.commentsCount = commentsCount;
        }

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public int getSubscribersCount() {
            return subscribersCount;
        }

        public void setSubscribersCount(int subscribersCount) {
            this.subscribersCount = subscribersCount;
        }

        public ScreenshotBean getScreenshot() {
            return screenshot;
        }

        public void setScreenshot(ScreenshotBean screenshot) {
            this.screenshot = screenshot;
        }

        public String getOriginalUrl() {
            return originalUrl;
        }

        public void setOriginalUrl(String originalUrl) {
            this.originalUrl = originalUrl;
        }

        public LatestCommentAtBean getLatestCommentAt() {
            return latestCommentAt;
        }

        public void setLatestCommentAt(LatestCommentAtBean latestCommentAt) {
            this.latestCommentAt = latestCommentAt;
        }

        public boolean isUserSubmit() {
            return userSubmit;
        }

        public void setUserSubmit(boolean userSubmit) {
            this.userSubmit = userSubmit;
        }

        public List<String> getTagsTitleArray() {
            return tagsTitleArray;
        }

        public void setTagsTitleArray(List<String> tagsTitleArray) {
            this.tagsTitleArray = tagsTitleArray;
        }

        public static class TagsBean {
            /**
             * __type : Relation
             * className : Tag
             */

            private String __type;
            private String className;

            public String get__type() {
                return __type;
            }

            public void set__type(String __type) {
                this.__type = __type;
            }

            public String getClassName() {
                return className;
            }

            public void setClassName(String className) {
                this.className = className;
            }
        }

        public static class SubscribersBean {
            /**
             * __type : Relation
             * className : _User
             */

            private String __type;
            private String className;

            public String get__type() {
                return __type;
            }

            public void set__type(String __type) {
                this.__type = __type;
            }

            public String getClassName() {
                return className;
            }

            public void setClassName(String className) {
                this.className = className;
            }
        }

        public static class EntryViewBean {
            /**
             * __type : Pointer
             * className : EntryView
             * objectId : 58ccd58cac502e005894f69b
             */

            private String __type;
            private String className;
            private String objectId;

            public String get__type() {
                return __type;
            }

            public void set__type(String __type) {
                this.__type = __type;
            }

            public String getClassName() {
                return className;
            }

            public void setClassName(String className) {
                this.className = className;
            }

            public String getObjectId() {
                return objectId;
            }

            public void setObjectId(String objectId) {
                this.objectId = objectId;
            }
        }

        public static class UserBean {
            /**
             * viewedEntriesCount : 829
             * role : editor
             * totalCollectionsCount : 9416
             * allowNotification : false
             * subscribedTagsCount : 2
             * appliedEditorAt : {"__type":"Date","iso":"2016-08-04T14:37:09.529Z"}
             * followersCount : 2646
             * updatedAt : 2017-03-18T12:12:04.837Z
             * postedEntriesCount : 25
             * latestCollectionUserNotification : {"__type":"Date","iso":"2017-03-18T06:19:42.723Z"}
             * commentedEntriesCount : 79
             * weeklyEmail : 0
             * collectedEntriesCount : 20
             * objectId : 57520855a341310063bd731f
             * postedPostsCount : 21
             * isAuthor : true
             * username : 亦枫
             * latestLoginedInAt : {"__type":"Date","iso":"2017-03-18T11:09:32.550Z"}
             * createdAt : 2016-06-03T22:44:37.465Z
             * totalHotIndex : 200744
             * blogAddress : http://yifeng.studio/
             * self_description : 个人微信公众号：安卓笔记侠（微信号：NiaoTech），博客地址：http://yifeng.studio/
             * latestCheckedNotificationAt : {"__type":"Date","iso":"2016-10-19T01:26:35.226Z"}
             * className : _User
             * emailVerified : false
             * totalCommentsCount : 251
             * installation : {"__type":"Pointer","className":"_Installation","objectId":"1BwPTNoShoYBhrdjtgvUtKwWu6fMxQSy"}
             * blacklist : false
             * mobilePhoneNumber : 18801772064
             * __type : Pointer
             * apply : false
             * followeesCount : 16
             * deviceType : android
             * avatar_hd : https://user-gold-cdn.xitu.io/2016/11/29/d88cabd86b9ab6c478121f9e15fb55a2
             * editorType : md
             * jobTitle : AndroidDeveloper
             * company : 微信公号：安卓笔记侠
             * authData : {"weibo":{"uid":"3137408085","expiration_in":"36000","access_token":"2.00NKP17DWKIDWD9dea41a965F1LzzC"},"github":{"access_token":"cfba2cc9b08809aaf8d9e952a2964f7f5faa24d8","expiration_in":"36000","uid":"8175587","html_url":"https://github.com/Mike-bel","bio":null,"username":"Mike-bel","nickname":"YiFeng","avatar_url":"https://avatars.githubusercontent.com/u/8175587?v=3"}}
             * avatar_large : https://dn-mhke0kuv.qbox.me/Bink3GWRzTiAzjGITIV1VHwGczJbr466RDX0lfI3
             * mobilePhoneVerified : true
             */

            private int viewedEntriesCount;
            private String role;
            private int totalCollectionsCount;
            private boolean allowNotification;
            private int subscribedTagsCount;
            private AppliedEditorAtBean appliedEditorAt;
            private int followersCount;
            private String updatedAt;
            private int postedEntriesCount;
            private LatestCollectionUserNotificationBean latestCollectionUserNotification;
            private int commentedEntriesCount;
            private int weeklyEmail;
            private int collectedEntriesCount;
            private String objectId;
            private int postedPostsCount;
            private boolean isAuthor;
            private String username;
            private LatestLoginedInAtBean latestLoginedInAt;
            private String createdAt;
            private int totalHotIndex;
            private String blogAddress;
            private String self_description;
            private LatestCheckedNotificationAtBean latestCheckedNotificationAt;
            private String className;
            private boolean emailVerified;
            private int totalCommentsCount;
            private InstallationBean installation;
            private boolean blacklist;
            private String mobilePhoneNumber;
            private String __type;
            private boolean apply;
            private int followeesCount;
            private String deviceType;
            private String avatar_hd;
            private String editorType;
            private String jobTitle;
            private String company;
            private AuthDataBean authData;
            private String avatar_large;
            private boolean mobilePhoneVerified;

            public int getViewedEntriesCount() {
                return viewedEntriesCount;
            }

            public void setViewedEntriesCount(int viewedEntriesCount) {
                this.viewedEntriesCount = viewedEntriesCount;
            }

            public String getRole() {
                return role;
            }

            public void setRole(String role) {
                this.role = role;
            }

            public int getTotalCollectionsCount() {
                return totalCollectionsCount;
            }

            public void setTotalCollectionsCount(int totalCollectionsCount) {
                this.totalCollectionsCount = totalCollectionsCount;
            }

            public boolean isAllowNotification() {
                return allowNotification;
            }

            public void setAllowNotification(boolean allowNotification) {
                this.allowNotification = allowNotification;
            }

            public int getSubscribedTagsCount() {
                return subscribedTagsCount;
            }

            public void setSubscribedTagsCount(int subscribedTagsCount) {
                this.subscribedTagsCount = subscribedTagsCount;
            }

            public AppliedEditorAtBean getAppliedEditorAt() {
                return appliedEditorAt;
            }

            public void setAppliedEditorAt(AppliedEditorAtBean appliedEditorAt) {
                this.appliedEditorAt = appliedEditorAt;
            }

            public int getFollowersCount() {
                return followersCount;
            }

            public void setFollowersCount(int followersCount) {
                this.followersCount = followersCount;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public int getPostedEntriesCount() {
                return postedEntriesCount;
            }

            public void setPostedEntriesCount(int postedEntriesCount) {
                this.postedEntriesCount = postedEntriesCount;
            }

            public LatestCollectionUserNotificationBean getLatestCollectionUserNotification() {
                return latestCollectionUserNotification;
            }

            public void setLatestCollectionUserNotification(LatestCollectionUserNotificationBean latestCollectionUserNotification) {
                this.latestCollectionUserNotification = latestCollectionUserNotification;
            }

            public int getCommentedEntriesCount() {
                return commentedEntriesCount;
            }

            public void setCommentedEntriesCount(int commentedEntriesCount) {
                this.commentedEntriesCount = commentedEntriesCount;
            }

            public int getWeeklyEmail() {
                return weeklyEmail;
            }

            public void setWeeklyEmail(int weeklyEmail) {
                this.weeklyEmail = weeklyEmail;
            }

            public int getCollectedEntriesCount() {
                return collectedEntriesCount;
            }

            public void setCollectedEntriesCount(int collectedEntriesCount) {
                this.collectedEntriesCount = collectedEntriesCount;
            }

            public String getObjectId() {
                return objectId;
            }

            public void setObjectId(String objectId) {
                this.objectId = objectId;
            }

            public int getPostedPostsCount() {
                return postedPostsCount;
            }

            public void setPostedPostsCount(int postedPostsCount) {
                this.postedPostsCount = postedPostsCount;
            }

            public boolean isIsAuthor() {
                return isAuthor;
            }

            public void setIsAuthor(boolean isAuthor) {
                this.isAuthor = isAuthor;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public LatestLoginedInAtBean getLatestLoginedInAt() {
                return latestLoginedInAt;
            }

            public void setLatestLoginedInAt(LatestLoginedInAtBean latestLoginedInAt) {
                this.latestLoginedInAt = latestLoginedInAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public int getTotalHotIndex() {
                return totalHotIndex;
            }

            public void setTotalHotIndex(int totalHotIndex) {
                this.totalHotIndex = totalHotIndex;
            }

            public String getBlogAddress() {
                return blogAddress;
            }

            public void setBlogAddress(String blogAddress) {
                this.blogAddress = blogAddress;
            }

            public String getSelf_description() {
                return self_description;
            }

            public void setSelf_description(String self_description) {
                this.self_description = self_description;
            }

            public LatestCheckedNotificationAtBean getLatestCheckedNotificationAt() {
                return latestCheckedNotificationAt;
            }

            public void setLatestCheckedNotificationAt(LatestCheckedNotificationAtBean latestCheckedNotificationAt) {
                this.latestCheckedNotificationAt = latestCheckedNotificationAt;
            }

            public String getClassName() {
                return className;
            }

            public void setClassName(String className) {
                this.className = className;
            }

            public boolean isEmailVerified() {
                return emailVerified;
            }

            public void setEmailVerified(boolean emailVerified) {
                this.emailVerified = emailVerified;
            }

            public int getTotalCommentsCount() {
                return totalCommentsCount;
            }

            public void setTotalCommentsCount(int totalCommentsCount) {
                this.totalCommentsCount = totalCommentsCount;
            }

            public InstallationBean getInstallation() {
                return installation;
            }

            public void setInstallation(InstallationBean installation) {
                this.installation = installation;
            }

            public boolean isBlacklist() {
                return blacklist;
            }

            public void setBlacklist(boolean blacklist) {
                this.blacklist = blacklist;
            }

            public String getMobilePhoneNumber() {
                return mobilePhoneNumber;
            }

            public void setMobilePhoneNumber(String mobilePhoneNumber) {
                this.mobilePhoneNumber = mobilePhoneNumber;
            }

            public String get__type() {
                return __type;
            }

            public void set__type(String __type) {
                this.__type = __type;
            }

            public boolean isApply() {
                return apply;
            }

            public void setApply(boolean apply) {
                this.apply = apply;
            }

            public int getFolloweesCount() {
                return followeesCount;
            }

            public void setFolloweesCount(int followeesCount) {
                this.followeesCount = followeesCount;
            }

            public String getDeviceType() {
                return deviceType;
            }

            public void setDeviceType(String deviceType) {
                this.deviceType = deviceType;
            }

            public String getAvatar_hd() {
                return avatar_hd;
            }

            public void setAvatar_hd(String avatar_hd) {
                this.avatar_hd = avatar_hd;
            }

            public String getEditorType() {
                return editorType;
            }

            public void setEditorType(String editorType) {
                this.editorType = editorType;
            }

            public String getJobTitle() {
                return jobTitle;
            }

            public void setJobTitle(String jobTitle) {
                this.jobTitle = jobTitle;
            }

            public String getCompany() {
                return company;
            }

            public void setCompany(String company) {
                this.company = company;
            }

            public AuthDataBean getAuthData() {
                return authData;
            }

            public void setAuthData(AuthDataBean authData) {
                this.authData = authData;
            }

            public String getAvatar_large() {
                return avatar_large;
            }

            public void setAvatar_large(String avatar_large) {
                this.avatar_large = avatar_large;
            }

            public boolean isMobilePhoneVerified() {
                return mobilePhoneVerified;
            }

            public void setMobilePhoneVerified(boolean mobilePhoneVerified) {
                this.mobilePhoneVerified = mobilePhoneVerified;
            }

            public static class AppliedEditorAtBean {
                /**
                 * __type : Date
                 * iso : 2016-08-04T14:37:09.529Z
                 */

                private String __type;
                private String iso;

                public String get__type() {
                    return __type;
                }

                public void set__type(String __type) {
                    this.__type = __type;
                }

                public String getIso() {
                    return iso;
                }

                public void setIso(String iso) {
                    this.iso = iso;
                }
            }

            public static class LatestCollectionUserNotificationBean {
                /**
                 * __type : Date
                 * iso : 2017-03-18T06:19:42.723Z
                 */

                private String __type;
                private String iso;

                public String get__type() {
                    return __type;
                }

                public void set__type(String __type) {
                    this.__type = __type;
                }

                public String getIso() {
                    return iso;
                }

                public void setIso(String iso) {
                    this.iso = iso;
                }
            }

            public static class LatestLoginedInAtBean {
                /**
                 * __type : Date
                 * iso : 2017-03-18T11:09:32.550Z
                 */

                private String __type;
                private String iso;

                public String get__type() {
                    return __type;
                }

                public void set__type(String __type) {
                    this.__type = __type;
                }

                public String getIso() {
                    return iso;
                }

                public void setIso(String iso) {
                    this.iso = iso;
                }
            }

            public static class LatestCheckedNotificationAtBean {
                /**
                 * __type : Date
                 * iso : 2016-10-19T01:26:35.226Z
                 */

                private String __type;
                private String iso;

                public String get__type() {
                    return __type;
                }

                public void set__type(String __type) {
                    this.__type = __type;
                }

                public String getIso() {
                    return iso;
                }

                public void setIso(String iso) {
                    this.iso = iso;
                }
            }

            public static class InstallationBean {
                /**
                 * __type : Pointer
                 * className : _Installation
                 * objectId : 1BwPTNoShoYBhrdjtgvUtKwWu6fMxQSy
                 */

                private String __type;
                private String className;
                private String objectId;

                public String get__type() {
                    return __type;
                }

                public void set__type(String __type) {
                    this.__type = __type;
                }

                public String getClassName() {
                    return className;
                }

                public void setClassName(String className) {
                    this.className = className;
                }

                public String getObjectId() {
                    return objectId;
                }

                public void setObjectId(String objectId) {
                    this.objectId = objectId;
                }
            }

            public static class AuthDataBean {
                /**
                 * weibo : {"uid":"3137408085","expiration_in":"36000","access_token":"2.00NKP17DWKIDWD9dea41a965F1LzzC"}
                 * github : {"access_token":"cfba2cc9b08809aaf8d9e952a2964f7f5faa24d8","expiration_in":"36000","uid":"8175587","html_url":"https://github.com/Mike-bel","bio":null,"username":"Mike-bel","nickname":"YiFeng","avatar_url":"https://avatars.githubusercontent.com/u/8175587?v=3"}
                 */

                private WeiboBean weibo;
                private GithubBean github;

                public WeiboBean getWeibo() {
                    return weibo;
                }

                public void setWeibo(WeiboBean weibo) {
                    this.weibo = weibo;
                }

                public GithubBean getGithub() {
                    return github;
                }

                public void setGithub(GithubBean github) {
                    this.github = github;
                }

                public static class WeiboBean {
                    /**
                     * uid : 3137408085
                     * expiration_in : 36000
                     * access_token : 2.00NKP17DWKIDWD9dea41a965F1LzzC
                     */

                    private String uid;
                    private String expiration_in;
                    private String access_token;

                    public String getUid() {
                        return uid;
                    }

                    public void setUid(String uid) {
                        this.uid = uid;
                    }

                    public String getExpiration_in() {
                        return expiration_in;
                    }

                    public void setExpiration_in(String expiration_in) {
                        this.expiration_in = expiration_in;
                    }

                    public String getAccess_token() {
                        return access_token;
                    }

                    public void setAccess_token(String access_token) {
                        this.access_token = access_token;
                    }
                }

                public static class GithubBean {
                    /**
                     * access_token : cfba2cc9b08809aaf8d9e952a2964f7f5faa24d8
                     * expiration_in : 36000
                     * uid : 8175587
                     * html_url : https://github.com/Mike-bel
                     * bio : null
                     * username : Mike-bel
                     * nickname : YiFeng
                     * avatar_url : https://avatars.githubusercontent.com/u/8175587?v=3
                     */

                    private String access_token;
                    private String expiration_in;
                    private String uid;
                    private String html_url;
                    private Object bio;
                    private String username;
                    private String nickname;
                    private String avatar_url;

                    public String getAccess_token() {
                        return access_token;
                    }

                    public void setAccess_token(String access_token) {
                        this.access_token = access_token;
                    }

                    public String getExpiration_in() {
                        return expiration_in;
                    }

                    public void setExpiration_in(String expiration_in) {
                        this.expiration_in = expiration_in;
                    }

                    public String getUid() {
                        return uid;
                    }

                    public void setUid(String uid) {
                        this.uid = uid;
                    }

                    public String getHtml_url() {
                        return html_url;
                    }

                    public void setHtml_url(String html_url) {
                        this.html_url = html_url;
                    }

                    public Object getBio() {
                        return bio;
                    }

                    public void setBio(Object bio) {
                        this.bio = bio;
                    }

                    public String getUsername() {
                        return username;
                    }

                    public void setUsername(String username) {
                        this.username = username;
                    }

                    public String getNickname() {
                        return nickname;
                    }

                    public void setNickname(String nickname) {
                        this.nickname = nickname;
                    }

                    public String getAvatar_url() {
                        return avatar_url;
                    }

                    public void setAvatar_url(String avatar_url) {
                        this.avatar_url = avatar_url;
                    }
                }
            }
        }

        public static class ScreenshotBean {
            /**
             * mime_type : image/jpeg
             * updatedAt : 2017-03-18T06:36:41.028Z
             * key : 03973e3eb1349923eb92.jpg
             * name : timg.jpg
             * objectId : 58ccd57961ff4b006025d87f
             * createdAt : 2017-03-18T06:36:41.028Z
             * __type : File
             * url : https://dn-mhke0kuv.qbox.me/03973e3eb1349923eb92.jpg
             * metaData : {"owner":"57520855a341310063bd731f","size":379258,"mime_type":"image/jpeg"}
             * bucket : mhke0kuv
             */

            private String mime_type;
            private String updatedAt;
            private String key;
            private String name;
            private String objectId;
            private String createdAt;
            private String __type;
            private String url;
            private MetaDataBean metaData;
            private String bucket;

            public String getMime_type() {
                return mime_type;
            }

            public void setMime_type(String mime_type) {
                this.mime_type = mime_type;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getObjectId() {
                return objectId;
            }

            public void setObjectId(String objectId) {
                this.objectId = objectId;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String get__type() {
                return __type;
            }

            public void set__type(String __type) {
                this.__type = __type;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public MetaDataBean getMetaData() {
                return metaData;
            }

            public void setMetaData(MetaDataBean metaData) {
                this.metaData = metaData;
            }

            public String getBucket() {
                return bucket;
            }

            public void setBucket(String bucket) {
                this.bucket = bucket;
            }

            public static class MetaDataBean {
                /**
                 * owner : 57520855a341310063bd731f
                 * size : 379258
                 * mime_type : image/jpeg
                 */

                private String owner;
                private int size;
                private String mime_type;

                public String getOwner() {
                    return owner;
                }

                public void setOwner(String owner) {
                    this.owner = owner;
                }

                public int getSize() {
                    return size;
                }

                public void setSize(int size) {
                    this.size = size;
                }

                public String getMime_type() {
                    return mime_type;
                }

                public void setMime_type(String mime_type) {
                    this.mime_type = mime_type;
                }
            }
        }

        public static class LatestCommentAtBean {
            /**
             * __type : Date
             * iso : 2017-03-18T09:04:04.495Z
             */

            private String __type;
            private String iso;

            public String get__type() {
                return __type;
            }

            public void set__type(String __type) {
                this.__type = __type;
            }

            public String getIso() {
                return iso;
            }

            public void setIso(String iso) {
                this.iso = iso;
            }
        }
//    }
}
