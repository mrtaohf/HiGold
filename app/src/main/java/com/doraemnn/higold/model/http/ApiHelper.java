package com.doraemnn.higold.model.http;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 描述：
 * <p>
 * Created by WK on 2017/3/1
 */
public class ApiHelper {


    public static HiGoldApi getService() {

        return retrofit(HiGoldApi.HOST).create(HiGoldApi.class);
    }


    /**
     * 获取 Retrofit
     *
     * @param baseUrl host地址
     * @return Retrofit
     */
    private static Retrofit retrofit(String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(OkHttpClientApi.getOkHttpClient())
                .build();
    }
}
