package com.doraemnn.higold.model.http;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by junwang on 2017/3/19.
 */

public class HeaderInterceptor implements Interceptor {


    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder requestBuilder = request.newBuilder()
                .header("X-LC-Sign","badc5461a25a46291054b902887a68eb,1480438132702")
                .header("X-LC-Id","mhke0kuv33myn4t4ghuid4oq2hjj12li374hvcif202y5bm6");

        Request build = requestBuilder.build();
        return chain.proceed(build);
    }
}
