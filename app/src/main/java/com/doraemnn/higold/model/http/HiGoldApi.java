package com.doraemnn.higold.model.http;

import com.doraemnn.higold.model.bean.BaseGoldBean;
import com.doraemnn.higold.model.bean.ExploreBannerBean;
import com.doraemnn.higold.model.bean.GoldResultBean;

import java.util.ArrayList;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * 描述：
 * <p>
 * Created by WK on 2017/3/1
 */
public interface HiGoldApi {

    String HOST = "https://api.leancloud.cn/";

    /*String HEADER_ID = "X-LC-Id:mhke0kuv33myn4t4ghuid4oq2hjj12li374hvcif202y5bm6";
    String HEADER_SIGN = "X-LC-Sign:badc5461a25a46291054b902887a68eb,1480438132702";

    /**
     * 获取文章列表
     */
    @GET("1.1/classes/Entry")
    Observable<BaseGoldBean<ArrayList<GoldResultBean>>> getGoldList(
            @Query("where") String where,
            @Query("order") String order,
            @Query("include") String include,
            @Query("limit") int limit,
            @Query("skip") int skip
    );

    /**
     * 发现页面---热门文章
     */
    @GET("1.1/clases/Entry")
    Observable<GoldResultBean> getGoldHotList(
            @Query("where") String where,
            @Query("order") String order,
            @Query("include") String include,
            @Query("limit") int limit
    );

    /**
     * 发现页面---banner
     *
     * @return
     */
    @GET("1.1/classes/Banner")
    Observable<BaseGoldBean<ArrayList<ExploreBannerBean>>> getBanner(
            @Query("where") String where,
            @Query("platform") String platform,
            @Query("position") String position,
            @Query("order") String order
    );

}
