package com.doraemnn.higold.model.http;

import com.doraemnn.higold.model.http.exception.ApiException;
import com.orhanobut.logger.Logger;

import rx.Subscriber;

/**
 * Created by junwang on 2017/3/19.
 */

public abstract class HttpSubscriber<T> extends Subscriber<T> {

    @Override
    public void onError(Throwable e) {
        Logger.d("异常参数"+e.getMessage());
        if (e instanceof ApiException) {
            error((ApiException) e);
        } else {
            error(new ApiException(e.getMessage(),0x1000));
        }
    }

    protected abstract void error(ApiException e);
}
