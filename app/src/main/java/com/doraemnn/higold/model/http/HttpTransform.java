package com.doraemnn.higold.model.http;

import com.doraemnn.higold.model.bean.BaseGoldBean;
import com.doraemnn.higold.model.http.exception.ApiException;
import com.orhanobut.logger.Logger;

import rx.Observable;
import rx.functions.Func1;

/**
 * 自定义transform转换器
 *
 * Created by junwang on 2017/3/19.
 */

public class HttpTransform<T> implements Observable.Transformer<BaseGoldBean<T>,T> {

    @Override
    public Observable<T> call(Observable<BaseGoldBean<T>> observable) {
        return observable
                .map(new Func1<BaseGoldBean<T>, BaseGoldBean<T>>() {
                    @Override
                    public BaseGoldBean<T> call(BaseGoldBean<T> response) {
                        if (response.getCode() != 0) {
                            throw new ApiException(response.getError(),response.getCode());
                        }
                        Logger.d("-----------HttpTransformer----------" + response.toString());
                        return response;
                    }
                })
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends BaseGoldBean<T>>>() {
                    @Override
                    public Observable<? extends BaseGoldBean<T>> call(Throwable throwable) {
                        throwable.printStackTrace();
                        return Observable.error(throwable);
                    }
                })
                .map(new Func1<BaseGoldBean<T>, T>() {
                    @Override
                    public T call(BaseGoldBean<T> tBaseGoldBean) {
                        return tBaseGoldBean.getResults();
                    }
                });
    }
}
