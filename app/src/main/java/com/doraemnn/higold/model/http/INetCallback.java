package com.doraemnn.higold.model.http;

import com.doraemnn.higold.model.http.exception.ApiException;

/**
 * 网络回调
 * Created by hongfei.tao on 2017/3/18.
 */
public interface INetCallback<T> {

    /**
     * 与服务器交互并成功返回后的回调
     */
    void onSuccess(T t);

    /**
     * 访问失败之后的回调
     */
    void onError(ApiException e);
}
