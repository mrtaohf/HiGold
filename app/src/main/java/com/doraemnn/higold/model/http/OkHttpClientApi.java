package com.doraemnn.higold.model.http;

import okhttp3.OkHttpClient;

/**
 * 描述：
 * <p>
 * Created by WK on 2017/3/1
 */
public class OkHttpClientApi {

    private static OkHttpClientApi instance = null;
    private static OkHttpClient mOkHttpClient;

    private OkHttpClientApi(OkHttpClient okHttpClient) {
        this.mOkHttpClient = okHttpClient;
    }

    public static OkHttpClientApi getInstance(OkHttpClient okHttpClient) {
        if (instance == null) {
            synchronized (OkHttpClientApi.class) {
                if (instance == null) {
                    instance = new OkHttpClientApi(okHttpClient);
                }
            }
        }
        return instance;
    }

    public static OkHttpClient getOkHttpClient() {

        return mOkHttpClient;
    }

}
