package com.doraemnn.higold.model.http.exception;

/**
 * API EXCEPTION
 * Created by junwang on 2017/3/19.
 */

public class ApiException extends RuntimeException{

    public final int code;
    public final String message;

    public ApiException(int code) {
        this(null,code);
    }

    public ApiException(String message, int code) {
        super(message);
        this.code = code;
        this.message = message;
    }
}
