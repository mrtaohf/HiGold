package com.doraemnn.higold.modules.details;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.doraemnn.higold.R;
import com.doraemnn.higold.app.Constants;
import com.doraemnn.higold.base.BaseActivity;
import com.doraemnn.higold.utils.GlideUtil;

import butterknife.BindView;

public class HiGoldDetailActivity extends BaseActivity<HiGoldDetailPresenter, HiGoldDetailModel> implements IDetailContract.IView {

    @BindView(R.id.iv_detail_imageView)
    ImageView mIvDetailImageView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout mAppBarLayout;
    @BindView(R.id.wv_webView)
    WebView mWebView;

    private String mDetailUrl;
    private String mTitle;
    private String mImageUrl;

    @Override
    protected void initPresenter() {

    }

    @Override
    protected void preInit(Bundle savedInstanceState) {
        mDetailUrl = getIntent().getStringExtra(Constants.BUNDLE_DETAIL_URL);
        mTitle = getIntent().getStringExtra(Constants.BUNDLE_DETAIL_TITLE);
        mImageUrl = getIntent().getStringExtra(Constants.BUNDLE_DETAIL_IMAGE);
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_hi_gold_detail;
    }

    @Override
    protected void findViews() {

    }

    @Override
    protected void initViews() {
        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(mTitle);
        }

        GlideUtil.displayBitmap(this, mIvDetailImageView, mImageUrl);

        if (!TextUtils.isEmpty(mDetailUrl)) {
            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.loadUrl(mDetailUrl);
            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                }
            });
        }
    }

    @Override
    protected void initComplete() {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
