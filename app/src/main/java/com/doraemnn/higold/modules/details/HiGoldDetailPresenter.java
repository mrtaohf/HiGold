package com.doraemnn.higold.modules.details;

import com.doraemnn.higold.base.BasePresenter;

/**
 * 详情页面Presenter
 * Created by hongfei.tao on 2017/4/16.
 */
class HiGoldDetailPresenter extends BasePresenter<IDetailContract.IView, IDetailContract.IModel> {
}
