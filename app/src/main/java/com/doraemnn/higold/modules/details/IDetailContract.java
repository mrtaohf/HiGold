package com.doraemnn.higold.modules.details;

import com.doraemnn.higold.base.BaseModel;

/**
 * 详情页面Contract
 * Created by hongfei.tao on 2017/4/16.
 */
interface IDetailContract {

    interface IModel extends BaseModel {

    }

    interface IView {

    }

    interface IPresenter {

    }

}
