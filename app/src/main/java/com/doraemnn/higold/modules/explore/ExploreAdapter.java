package com.doraemnn.higold.modules.explore;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.doraemnn.higold.R;
import com.doraemnn.higold.model.bean.ExploreBannerBean;
import com.doraemnn.higold.model.bean.GoldResultBean;
import com.doraemnn.higold.widgets.banner.Banner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 当前类注释：发现页面
 * <p>
 * Author :LeonWang <p>
 * Created  2017/4/1.15:17 <p>
 * Description:
 * <p>
 * E-mail:lijiawangjun@gmail.com
 */

public class ExploreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    //header banner
    private static final int HEADER_ITEM = 0;
    //category
    private static final int CATEGORY_ITEM = 1;
    //沸点
    private static final int FEIDIAN_ITEM = 2;
    //热门文章
    private static final int HOT_ITEM = 3;
    //底部刷新
    private static final int FOOTER_ITEM = 4;
    private final LayoutInflater mInflater;

    private List<ExploreBannerBean> mBannerList ;
    private List<GoldResultBean> mHotDataList;

    public ExploreAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        mBannerList = new ArrayList<>();
        mHotDataList = new ArrayList<>();
    }

    public void setHeaderData(List<ExploreBannerBean> bannerList) {
        if (bannerList != null) {
            mBannerList.addAll(bannerList);
            notifyDataSetChanged();
        }
    }

    public void setHotDataList(List<GoldResultBean> hotDataList) {
        if (hotDataList != null) {
            mHotDataList.addAll(hotDataList);
            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_ITEM) {
            View bannerView = mInflater.inflate(R.layout.explore_banner_item, parent, false);
            return new HeaderViewHolder(bannerView);
        } else if (viewType == CATEGORY_ITEM) {
            View categoryView = mInflater.inflate(R.layout.explore_category_item, parent, false);
            return new CategoryViewHolder(categoryView);
        } else if (viewType == FEIDIAN_ITEM) {
            View feidianView = mInflater.inflate(R.layout.explore_hot_item, parent, false);
            return new FeidianViewHolder(feidianView);
        } else if (viewType == FOOTER_ITEM) {
            View footerView = mInflater.inflate(R.layout.item_footer_view, parent, false);
            return new FooterViewHolder(footerView);
        } else {
            View hotTopicView = mInflater.inflate(R.layout.item_normal_view, parent, false);
            return new HotTopicViewHolder(hotTopicView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderViewHolder && mBannerList.size() != 0) {
            initHeaderData((HeaderViewHolder)holder,mBannerList);
        } else if (holder instanceof HotTopicViewHolder && mHotDataList.size() != 0) {
            initHotTopicData((HotTopicViewHolder)holder,mHotDataList);
        } else if (holder instanceof CategoryViewHolder) {
            initCategoryData((CategoryViewHolder)holder);
        } else if (holder instanceof FooterViewHolder) {
            initFooterData((FooterViewHolder) holder);
        } else if (holder instanceof FeidianViewHolder) {
            initFeidianData((FeidianViewHolder)holder);
        }
    }


    /**
     * 沸点
     * @param holder
     */
    private void initFeidianData(FeidianViewHolder holder) {

    }


    /**
     * footer
     * @param holder
     */
    private void initFooterData(FooterViewHolder holder) {

    }


    /**
     * 分类
     * @param holder
     */
    private void initCategoryData(CategoryViewHolder holder) {

    }


    /**
     * 加载热门文章数据
     * @param holder
     * @param hotDataList
     */
    private void initHotTopicData(HotTopicViewHolder holder, List<GoldResultBean> hotDataList) {

    }


    /**
     * 加载轮播图数据
     * @param holder
     * @param bannerList
     */
    private void initHeaderData(HeaderViewHolder holder, List<ExploreBannerBean> bannerList) {

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return HEADER_ITEM;
        else if (position == 1) return CATEGORY_ITEM;
        else if (position == 2) return FEIDIAN_ITEM;
        else if (position + 1 == getItemCount()) return FOOTER_ITEM;
        else return HOT_ITEM;
    }

    @Override
    public int getItemCount() {
        return 0;
    }


    /**
     * banner
     */
    static class HeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.banner)
        Banner mBanner;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * category
     */
    static class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_week_hot)
        AppCompatTextView mTvWeekHot;
        @BindView(R.id.tv_cllection)
        AppCompatTextView mTvCollection;
        @BindView(R.id.tv_activities)
        AppCompatTextView mTvActiviti;
        @BindView(R.id.tv_section)
        AppCompatTextView mTvSection;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * 沸点
     */
    static class FeidianViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.rcv_hot)
        RecyclerView mRcvHot;

        public FeidianViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    /**
     * 热门文章
     */
    static class HotTopicViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        AppCompatTextView mTvTitle;
        @BindView(R.id.tv_article_info)
        AppCompatTextView mTvArtCleInfo;
        @BindView(R.id.iv_imageView)
        AppCompatImageView mIvImage;

        public HotTopicViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }


    /**
     * footer refresh
     */
    static class FooterViewHolder extends RecyclerView.ViewHolder {

        public FooterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
