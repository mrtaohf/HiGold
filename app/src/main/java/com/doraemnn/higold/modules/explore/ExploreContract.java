package com.doraemnn.higold.modules.explore;

import com.doraemnn.higold.base.BaseModel;
import com.doraemnn.higold.base.BasePresenter;
import com.doraemnn.higold.base.IBaseView;

/**
 * 当前类注释：发现页面 Contract
 * <p>
 * Author :LeonWang <p>
 * Created  2017/3/20.11:30 <p>
 * Description:
 * <p>
 * E-mail:lijiawangjun@gmail.com
 */

public interface ExploreContract {

    /**
     * model
     */
    interface Model extends BaseModel {

    }

    /**
     * view
     */
    interface View extends IBaseView {}


    /**
     * presenter
     */
    abstract static class Presenter extends BasePresenter<View, Model> {}

}
