package com.doraemnn.higold.modules.explore;

import android.graphics.Color;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.doraemnn.higold.R;
import com.doraemnn.higold.base.BaseFragment;
import com.doraemnn.higold.model.http.exception.ApiException;
import com.doraemnn.higold.utils.ScreenUtil;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import butterknife.BindView;

/**
 * 描述：发现页面
 * <p>
 * Created by WK on 2017/3/1
 *
 * @update leonWang 2017/03/20
 */
public class ExploreFragment extends BaseFragment<ExplorePresenter, ExploreModel> implements
        ExploreContract.View,
        SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.rl_toolbar)
    RelativeLayout mToolbar;
    @BindView(R.id.explore_recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.refrsh_layout)
    SwipeRefreshLayout mRefreshLayout;
    private LinearLayoutManager mLinearLayoutManager;
    private ExploreAdapter mAdapter;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_explore;
    }

    @Override
    protected void initPresenter() {
        mPresenter.setVM(this, mModel);
    }

    @Override
    protected void initViews() {
        initStatusBarHeight();
    }

    private void initStatusBarHeight() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int statusBarHeight = mToolbar.getPaddingTop() + ScreenUtil.getStatusBarHeight();
            mToolbar.setPadding(0, statusBarHeight, 0, 0);
        }
    }


    @Override
    protected void findViews(View view) {
        mRefreshLayout.setOnRefreshListener(this);
        if (mLinearLayoutManager == null)
            mLinearLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(mContext)
                        .color(Color.TRANSPARENT)
                        .sizeResId(R.dimen.dimen_space_10)
                        .build());
        if (mAdapter == null)
            mAdapter = new ExploreAdapter(mContext);
        mRecyclerView.setAdapter(mAdapter);
        showLoading();
    }

    @Override
    protected void loadData() {

    }

    @Override
    public void showLoading() {
        mRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
            }
        },1000);
    }

    @Override
    public void showErrorView(ApiException e) {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void onRefresh() {

    }
}
