package com.doraemnn.higold.modules.home;

import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.doraemnn.higold.R;
import com.doraemnn.higold.base.BaseFragment;
import com.doraemnn.higold.utils.ScreenUtil;

import butterknife.BindView;

/**
 * 描述：
 * <p>
 * Created by WK on 2017/3/1
 */
public class HomeFragment extends BaseFragment {

    @BindView(R.id.ll_header_container)
    LinearLayout mLlContainer;
    @BindView(R.id.tab_container)
    TabLayout mTabContainer;
    @BindView(R.id.vp_container)
    ViewPager mVpContainer;

    private HomeTabPagerAdapter mPagerAdapter;

    @Override
    protected void initPresenter() {

    }

    @Override
    protected void initViews() {
        initStatusBarHeight();
        if (mPagerAdapter == null)
            mPagerAdapter = new HomeTabPagerAdapter(getChildFragmentManager());
        mVpContainer.setAdapter(mPagerAdapter);
        mTabContainer.setupWithViewPager(mVpContainer);

        setTextAllCaps(mTabContainer);
    }

    private void setTextAllCaps(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            if (viewGroup.getChildCount() > 0) {
                for (int i = 0; i < viewGroup.getChildCount(); i++) {
                    View childView = viewGroup.getChildAt(i);
                    setTextAllCaps(childView);
                }
            }
        } else {
            if (view instanceof TextView) {
                ((TextView) view).setAllCaps(false);
            }
        }
    }

    private void initStatusBarHeight() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int statusBarHeight = mLlContainer.getPaddingTop() + ScreenUtil.getStatusBarHeight();
            mLlContainer.setPadding(0, statusBarHeight, 0, 0);
        }
    }

    @Override
    protected int layoutResId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void findViews(View view) {

    }

    @Override
    protected void loadData() {

    }

}
