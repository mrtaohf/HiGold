package com.doraemnn.higold.modules.home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.doraemnn.higold.R;
import com.doraemnn.higold.modules.home.tab.HomeTabFragment;
import com.doraemnn.higold.utils.ContextUtil;

import java.util.List;

/**
 * 首页Tab页的PagerAdapter
 * Created by hongfei.tao on 2017/3/17 19:59.
 */
class HomeTabPagerAdapter extends FragmentPagerAdapter {

    private List<String> mTabTitles;

    HomeTabPagerAdapter(FragmentManager fm) {
        super(fm);
        this.mTabTitles = ContextUtil.getStringArray(R.array.home_tabs);
    }

    @Override
    public Fragment getItem(int position) {
        return HomeTabFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return mTabTitles == null ? 0 : mTabTitles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabTitles.get(position);
    }
}
