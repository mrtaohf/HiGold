package com.doraemnn.higold.modules.home.tab;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.doraemnn.higold.R;
import com.doraemnn.higold.model.bean.GoldResultBean;
import com.doraemnn.higold.utils.ContextUtil;
import com.doraemnn.higold.utils.GlideUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 配合RecyclerView使用，可以加载更多
 * Created by hongfei.tao on 2017/3/18.
 */
class HomeItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE = 0;
    private static final int FOOTER_TYPE = 1;

    private Context mContext;

    private final Object mLock = new Object();
    private OnItemClickListener<GoldResultBean> mItemClickListener;

    private List<GoldResultBean> mDataList;

    private Fragment mFragment;

    HomeItemAdapter(Fragment fragment) {
        this.mContext = ContextUtil.getContext();
        this.mFragment = fragment;
        mDataList = new ArrayList<>();
    }

    void setDataList(List<GoldResultBean> list) {
        if (list != null) {
            synchronized (mLock) {
                mDataList.clear();
                mDataList.addAll(list);
            }

            notifyDataSetChanged();
        }
    }

    void addAll(List<GoldResultBean> list) {
        if (list != null && list.size() > 0) {
            int positionStart = getItemCount() - 1;
            int size = list.size();

            synchronized (mLock) {
                mDataList.addAll(list);
            }

            notifyItemRangeInserted(positionStart, size);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == FOOTER_TYPE) {
            return new FooterViewHolder(View.inflate(mContext, R.layout.item_footer_view, null));
        } else {
            return new HomeItemViewHolder(View.inflate(mContext, R.layout.item_normal_view, null));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (getItemViewType(position) == ITEM_TYPE) {

            final GoldResultBean bean = mDataList.get(position);
            String title = bean.getTitle();
            String username = null;
            if (bean.getUser() != null) {
                username = bean.getUser().getUsername();
            }
            int collectionCount = bean.getCollectionCount();

            GoldResultBean.ScreenshotBean screenshot = bean.getScreenshot();
            String imageUrl = null;
            if (screenshot != null) {
                imageUrl = screenshot.getUrl();
            }

            String info = collectionCount + "人喜欢";
            if (!TextUtils.isEmpty(username))
                info = info.concat("·").concat(username);

            if (viewHolder instanceof HomeItemViewHolder) {

                final HomeItemViewHolder holder = (HomeItemViewHolder) viewHolder;

                holder.mTvTitle.setText(title);
                holder.mTvArticleInfo.setText(info);
                if (!TextUtils.isEmpty(imageUrl)) {
                    holder.mIvImageView.setVisibility(View.VISIBLE);
                    GlideUtil.displayBitmap(this.mFragment, holder.mIvImageView, imageUrl);
                } else
                    holder.mIvImageView.setVisibility(View.GONE);

                holder.mItemContainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mItemClickListener != null)
                            mItemClickListener.onItemClick(holder.getAdapterPosition(), bean);
                    }
                });
            }
        }
    }


    @Override
    public int getItemCount() {
        return mDataList == null || mDataList.isEmpty() ? 0 : mDataList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position + 1 == getItemCount()) {
            return FOOTER_TYPE;
        } else {
            return ITEM_TYPE;
        }
    }

    static class HomeItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_container)
        RelativeLayout mItemContainer;
        @BindView(R.id.tv_title)
        AppCompatTextView mTvTitle;
        @BindView(R.id.tv_article_info)
        AppCompatTextView mTvArticleInfo;
        @BindView(R.id.iv_imageView)
        AppCompatImageView mIvImageView;

        HomeItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private static class FooterViewHolder extends RecyclerView.ViewHolder {

        FooterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setOnItemClickListener(OnItemClickListener<GoldResultBean> listener) {
        this.mItemClickListener = listener;
    }

    /**
     * 条目点击监听
     */
    public interface OnItemClickListener<T> {
        void onItemClick(int position, T itemData);
    }
}
