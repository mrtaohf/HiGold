package com.doraemnn.higold.modules.home.tab;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.doraemnn.higold.R;
import com.doraemnn.higold.app.Constants;
import com.doraemnn.higold.base.BaseFragment;
import com.doraemnn.higold.model.bean.GoldResultBean;
import com.doraemnn.higold.model.http.exception.ApiException;
import com.doraemnn.higold.modules.details.HiGoldDetailActivity;

import java.util.ArrayList;

import butterknife.BindView;

import static com.doraemnn.higold.R.id.recyclerView;

/**
 * 首页Tab页面
 * Created by hongfei.tao on 2017/3/18.
 */
public class HomeTabFragment extends BaseFragment implements IContract.IView, SwipeRefreshLayout.OnRefreshListener {

    @BindView(recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private IContract.IPresenter mPresenter;

    private boolean mIsFirstLoad = true;
    private boolean mOnLoading = false;

    private HomeItemAdapter mItemAdapter;
    private LinearLayoutManager mLayoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int position = getArguments() != null ? getArguments().getInt(Constants.BUNDLE_DETAIL_URL) : 0;

        this.mPresenter = new HomeTabPresenter(this);
        this.mPresenter.handleParams(position);
    }


    @Override
    protected void initPresenter() {

    }

    @Override
    protected void initViews() {
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastPosition = mLayoutManager.findLastVisibleItemPosition();

                if (!mOnLoading && lastPosition == mItemAdapter.getItemCount() - 1) {
                    onLoadMore();
                }
            }
        });

        if (mItemAdapter == null)
            mItemAdapter = new HomeItemAdapter(this);
        mRecyclerView.setAdapter(mItemAdapter);
        mItemAdapter.setOnItemClickListener(new HomeItemAdapter.OnItemClickListener<GoldResultBean>() {
            @Override
            public void onItemClick(int position, GoldResultBean itemData) {
                String detailUrl = itemData.getUrl();
                String title = itemData.getTitle();
                GoldResultBean.ScreenshotBean screenshot = itemData.getScreenshot();
                String imageUrl = null;
                if (screenshot != null) {
                    imageUrl = screenshot.getUrl();
                }

                Intent intent = new Intent(mContext, HiGoldDetailActivity.class);
                intent.putExtra(Constants.BUNDLE_DETAIL_URL, detailUrl);
                intent.putExtra(Constants.BUNDLE_DETAIL_IMAGE, imageUrl);
                intent.putExtra(Constants.BUNDLE_DETAIL_TITLE, title);
                mContext.startActivity(intent);
            }
        });

        if (mLayoutManager == null)
            mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(
                mRecyclerView.getContext(),
                mLayoutManager.getOrientation())
        );

        showLoading();
    }

    @Override
    protected int layoutResId() {
        return R.layout.fragment_home_tab;
    }

    @Override
    protected void findViews(View view) {

    }

    @Override
    protected void loadData() {
        if (mIsFirstLoad) {
            mPresenter.startRequest();
            mIsFirstLoad = false;
        }
    }

    public static Fragment newInstance(int position) {
        HomeTabFragment fragment = new HomeTabFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(Constants.BUNDLE_DETAIL_URL, position);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onRefresh() {
        mPresenter.refreshRequest();
    }


    @Override
    public void showLoading() {
        mSwipeRefreshLayout.measure(0, 0);
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void onLoadMore() {
        mOnLoading = true;
        mPresenter.loadMore();
    }

    @Override
    public void setListView(ArrayList<GoldResultBean> list, boolean isShouldClear) {
        if (isAdded()) {
            mSwipeRefreshLayout.setRefreshing(false);

            if (isShouldClear)
                mItemAdapter.setDataList(list);
            else
                mItemAdapter.addAll(list);
        }
    }

    @Override
    public void showErrorView(ApiException e) {
        if (isAdded()) {
            toast(R.string.data_load_error);

            mOnLoading = false;
            if (mSwipeRefreshLayout.isRefreshing())
                mSwipeRefreshLayout.setRefreshing(false);

            if (mLayoutManager.findLastVisibleItemPosition() == mItemAdapter.getItemCount()) {
                mLayoutManager.scrollToPosition(mItemAdapter.getItemCount() - 1);
            }
        }

    }

    @Override
    public void hideLoading() {
        if (isAdded()) {
            mOnLoading = false;
            mSwipeRefreshLayout.setRefreshing(false);

            if (mLayoutManager.findLastVisibleItemPosition() == mItemAdapter.getItemCount()) {
                mLayoutManager.scrollToPosition(mItemAdapter.getItemCount() - 1);
            }
        }
    }
}
