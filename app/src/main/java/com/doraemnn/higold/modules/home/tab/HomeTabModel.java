package com.doraemnn.higold.modules.home.tab;

import android.text.TextUtils;

import com.doraemnn.higold.app.Constants;
import com.doraemnn.higold.model.bean.GoldResultBean;
import com.doraemnn.higold.model.http.ApiHelper;
import com.doraemnn.higold.model.http.HttpSubscriber;
import com.doraemnn.higold.model.http.HttpTransform;
import com.doraemnn.higold.model.http.exception.ApiException;

import java.util.ArrayList;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 主页面的Tab页-Model
 * Created by hongfei.tao on 2017/3/18.
 */
public class HomeTabModel implements IContract.IModel {

    private IContract.IPresenter mPresenter;

    private String mParams;
    private int mPage = 0;
    private int mSkipSize = mPage * Constants.PAGE_SIZE;

    public HomeTabModel(IContract.IPresenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setParams(String params) {
        if (TextUtils.equals("home", params)) {
            this.mParams = "{}";
        } else {
            this.mParams = "{\"category\":\"" + params + "\"}";
        }
    }

    @Override
    public void setCurrPage(int page) {
        this.mPage = page;
        this.mSkipSize = Constants.PAGE_SIZE * mPage;
    }

    @Override
    public int getCurrPage() {
        return this.mPage;
    }

    @Override
    public void sendRequest() {
        if (!TextUtils.isEmpty(mParams)) {
            Subscription rxSubscription = ApiHelper.getService()
                    .getGoldList(mParams, "-createdAt", "user", Constants.PAGE_SIZE, mSkipSize)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .compose(new HttpTransform<ArrayList<GoldResultBean>>())
                    .subscribe(new HttpSubscriber<ArrayList<GoldResultBean>>() {
                        @Override
                        protected void error(ApiException e) {
                            mPresenter.onError(e);
                        }

                        @Override
                        public void onCompleted() {
                            mPresenter.onNetCompleted();
                        }

                        @Override
                        public void onNext(ArrayList<GoldResultBean> goldResultBeen) {
                            mPresenter.onSuccess(goldResultBeen);
                        }
                    });
        }
    }
}
