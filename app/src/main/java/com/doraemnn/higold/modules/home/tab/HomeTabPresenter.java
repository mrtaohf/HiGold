package com.doraemnn.higold.modules.home.tab;

import com.doraemnn.higold.R;
import com.doraemnn.higold.model.bean.GoldResultBean;
import com.doraemnn.higold.model.http.exception.ApiException;
import com.doraemnn.higold.utils.ContextUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 主页面的Tab页-Presenter
 * Created by hongfei.tao on 2017/3/18.
 */
public class HomeTabPresenter implements IContract.IPresenter {

    private IContract.IView mView;
    private IContract.IModel mModel;

    public HomeTabPresenter(IContract.IView view) {
        this.mView = view;
        this.mModel = new HomeTabModel(this);
    }

    @Override
    public void handleParams(int position) {
        List<String> paramList = ContextUtil.getStringArray(R.array.home_tab_params);
        this.mModel.setParams(paramList.get(position));
    }

    @Override
    public void startRequest() {
        this.mModel.sendRequest();
    }

    @Override
    public void onNetCompleted() {
        mView.hideLoading();
    }

    @Override
    public void refreshRequest() {
        mModel.setCurrPage(0);
        mModel.sendRequest();
    }

    @Override
    public void loadMore() {
        mModel.sendRequest();
    }

    @Override
    public void onSuccess(ArrayList<GoldResultBean> goldResultBean) {
        this.mModel.setCurrPage(this.mModel.getCurrPage() + 1);
        mView.setListView(goldResultBean, this.mModel.getCurrPage() == 1);
    }

    @Override
    public void onError(ApiException e) {
        mView.showErrorView(e);
    }

}
