package com.doraemnn.higold.modules.home.tab;

import com.doraemnn.higold.base.IBaseView;
import com.doraemnn.higold.model.bean.GoldResultBean;
import com.doraemnn.higold.model.http.INetCallback;

import java.util.ArrayList;

/**
 * 主页面的Tab页-Contract
 * Created by hongfei.tao on 2017/3/18.
 */
interface IContract {

    interface IModel {

        /**
         * 设置页面请求需要的参数
         *
         * @param params 参数Category
         */
        void setParams(String params);

        /**
         * 设置下次请求的分页数index
         *
         * @param page 分页
         */
        void setCurrPage(int page);

        /**
         * 获取当前分页
         *
         * @return int 分页
         */
        int getCurrPage();

        /**
         * 获取网络数据
         */
        void sendRequest();

    }

    interface IView extends IBaseView {

        /**
         * 加载更多的回调
         */
        void onLoadMore();

        /**
         * 页面数据展示
         *
         * @param listBean 展示需要的数据
         */
        void setListView(ArrayList<GoldResultBean> listBean, boolean shouldClear);

    }

    interface IPresenter extends INetCallback<ArrayList<GoldResultBean>> {

        /**
         * 根据传进来的页面索引值，获取页面的请求参数
         *
         * @param position 页面索引值
         */
        void handleParams(int position);

        /**
         * 发起网络请求
         */
        void startRequest();

        /**
         * 网络数据请求结束之后的操作
         */
        void onNetCompleted();

        /**
         * 刷新页面后的操作
         */
        void refreshRequest();

        /**
         * 加载更多
         */
        void loadMore();
    }

}
