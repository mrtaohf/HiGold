package com.doraemnn.higold.modules.notification;

import android.view.View;

import com.doraemnn.higold.R;
import com.doraemnn.higold.base.BaseFragment;

/**
 * 描述：
 * <p>
 * Created by WK on 2017/3/1
 */
public class NotificationFragment extends BaseFragment {


    @Override
    protected void initPresenter() {

    }

    @Override
    protected void initViews() {

    }

    @Override
    protected int layoutResId() {
        return R.layout.fragment_notification;
    }

    @Override
    protected void findViews(View view) {

    }

    @Override
    protected void loadData() {

    }

}
