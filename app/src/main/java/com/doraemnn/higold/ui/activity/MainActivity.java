package com.doraemnn.higold.ui.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;

import com.doraemnn.higold.R;
import com.doraemnn.higold.base.BaseTranslucentActivity;
import com.doraemnn.higold.modules.explore.ExploreFragment;
import com.doraemnn.higold.modules.home.HomeFragment;
import com.doraemnn.higold.modules.notification.NotificationFragment;
import com.doraemnn.higold.modules.profile.ProfileFragment;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseTranslucentActivity implements View.OnClickListener {

    @BindView(R.id.tab_home)
    ImageView mTabHome;
    @BindView(R.id.tab_explore)
    ImageView mTabExplore;
    @BindView(R.id.tab_notification)
    ImageView mTabNotification;
    @BindView(R.id.tab_profile)
    ImageView mTabProfile;

    private ProfileFragment mProfileFragment;
    private HomeFragment mHomeFragment;
    private ExploreFragment mExploreFragment;
    private NotificationFragment mNotificationFragment;

    private FragmentManager mFragmentManager;

    private int lastPosition = -1;

    @Override
    protected void initPresenter() {

    }

    @Override
    protected void preInit(Bundle savedInstanceState) {

    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void findViews() {
        mTabHome = (ImageView) findViewById(R.id.tab_home);
        mTabExplore = (ImageView) findViewById(R.id.tab_explore);
        mTabNotification = (ImageView) findViewById(R.id.tab_notification);
        mTabProfile = (ImageView) findViewById(R.id.tab_profile);
    }

    @Override
    protected void initViews() {
        setUniversalToolbar("主页");
    }


    @Override
    protected void initComplete() {
        mFragmentManager = getSupportFragmentManager();
        mHomeFragment = (HomeFragment) mFragmentManager.findFragmentById(
                R.id.home_fragment);
        mExploreFragment = (ExploreFragment) mFragmentManager.findFragmentById(
                R.id.exolore_fragment);
        mNotificationFragment =
                (NotificationFragment) mFragmentManager.findFragmentById(
                        R.id.notification_fragment);
        mProfileFragment = (ProfileFragment) mFragmentManager.findFragmentById(
                R.id.profile_fragment);
        selectTab(0);
    }

    //4 Tab 切换
    @OnClick({R.id.tab_home, R.id.tab_explore, R.id.tab_notification, R.id.tab_profile})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tab_home:
                selectTab(0);
                break;
            case R.id.tab_explore:
                selectTab(1);
                break;
            case R.id.tab_notification:
                selectTab(2);
                break;
            case R.id.tab_profile:
                selectTab(3);
                break;
        }
    }

    private void selectTab(int position) {
        FragmentTransaction fragmentTransaction =
                mFragmentManager.beginTransaction();
        switch (lastPosition) {
            case 0:
                mTabHome.setEnabled(true);
                fragmentTransaction.hide(mHomeFragment);
                break;
            case 1:
                mTabExplore.setEnabled(true);
                fragmentTransaction.hide(mExploreFragment);
                break;
            case 2:
                mTabNotification.setEnabled(true);
                fragmentTransaction.hide(mNotificationFragment);
                break;
            case 3:
                mTabProfile.setEnabled(true);
                fragmentTransaction.hide(mProfileFragment);
                break;
            default:
                mTabExplore.setEnabled(true);
                mTabNotification.setEnabled(true);
                mTabProfile.setEnabled(true);
                fragmentTransaction.hide(mExploreFragment);
                fragmentTransaction.hide(mNotificationFragment);
                fragmentTransaction.hide(mProfileFragment);
                break;
        }
        lastPosition = position;
        switch (lastPosition) {
            case 0:
                mTabHome.setEnabled(false);
                fragmentTransaction.show(mHomeFragment);
                break;
            case 1:
                mTabExplore.setEnabled(false);
                fragmentTransaction.show(mExploreFragment);
                break;
            case 2:
                mTabNotification.setEnabled(false);
                fragmentTransaction.show(mNotificationFragment);
                break;
            case 3:
                mTabProfile.setEnabled(false);
                fragmentTransaction.show(mProfileFragment);
                break;
        }
        fragmentTransaction.commit();
    }


}
