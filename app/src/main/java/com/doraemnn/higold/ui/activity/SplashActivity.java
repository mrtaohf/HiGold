package com.doraemnn.higold.ui.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.doraemnn.higold.R;
import com.doraemnn.higold.app.Constants;
import com.doraemnn.higold.utils.SPUtil;
import com.doraemnn.higold.utils.permission.PerUtils;
import com.doraemnn.higold.utils.permission.PerimissionsCallback;
import com.doraemnn.higold.utils.permission.PermissionEnum;
import com.doraemnn.higold.utils.permission.PermissionManager;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * 描述：
 * <p>
 * Created by WK on 2017/3/1
 */
public class SplashActivity extends AppCompatActivity {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //判断是否第一次进入应用
        boolean isFirstOpen = SPUtil.getBoolean(this, Constants.GUIDE_IS_OPEN, true);
        if (isFirstOpen) {
            startToActivity(GuideActivity.class);
            finish();
            return;
        }
        //如果不是第一次启动app,则正常显示启动页
        setContentView(R.layout.activity_splash);
        mContext = this;
        checkPermissions();
//        displayDelay();
    }


    /**
     * 权限检查
     */
    private void checkPermissions() {
        PermissionManager
                .with(SplashActivity.this)
                .tag(1000)
                .permission(PermissionEnum.READ_EXTERNAL_STORAGE, PermissionEnum.WRITE_EXTERNAL_STORAGE, PermissionEnum.CAMERA)
                .callback(new PerimissionsCallback() {
                    @Override
                    public void onGranted(ArrayList<PermissionEnum> grantedList) {
                        displayDelay();
                    }

                    @Override
                    public void onDenied(ArrayList<PermissionEnum> deniedList) {
//                            Toast.makeText(SplashActivity.this,"权限被拒绝",Toast.LENGTH_SHORT).show();
                        PermissionDenied(deniedList);
                    }
                })
                .checkAsk();
    }

    private void PermissionDenied(final ArrayList<PermissionEnum> permissionsDenied) {
        StringBuilder msgCN = new StringBuilder();
        for (int i = 0; i < permissionsDenied.size(); i++) {

            if (i == permissionsDenied.size() - 1) {
                msgCN.append(permissionsDenied.get(i).getName_cn());
            } else {
                msgCN.append(permissionsDenied.get(i).getName_cn() + ",");
            }
        }
        if (mContext == null) {
            return;
        }

        TextView textView = new TextView(mContext);
        textView.setPadding(40, 30, 40, 20);
        textView.setTextSize(15);
        textView.setTextColor(getResources().getColor(R.color.colorPrimary));
        textView.setText(String.format(mContext.getResources().getString(R.string.permission_explain), msgCN.toString()));

        AlertDialog alertDialog = new AlertDialog.Builder(mContext)
                .setView(textView)
                .setCancelable(false)
                .setPositiveButton(R.string.per_setting, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PerUtils.openApplicationSettings(mContext, R.class.getPackage().getName());
                    }
                })
                .setNegativeButton(R.string.per_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startToActivity(MainActivity.class);
                        finish();
                    }
                }).create();
        alertDialog.show();
    }

    private void displayDelay() {
        Observable.interval(2, TimeUnit.SECONDS).take(1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {
                        startToActivity(MainActivity.class);
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Long aLong) {

                    }
                });
    }

    public void startToActivity(Class<?> clazz) {
        this.startActivity(new Intent(SplashActivity.this, clazz));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionManager.handleResult(requestCode, permissions, grantResults);
    }
}
