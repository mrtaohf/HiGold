package com.doraemnn.higold.utils;

import java.lang.reflect.ParameterizedType;

/**
 * 当前类注释：类初始化转化
 * <p>
 * Author :LeonWang <p>
 * Created  2017/3/20.10:53 <p>
 * Description:
 * <p>
 * E-mail:lijiawangjun@gmail.com
 */

public class ClazzFormatUtil {

    public static <T> T getT(Object o,int i) {
        try {
            return ((Class<T>)((ParameterizedType)(o.getClass().getGenericSuperclass())).getActualTypeArguments()[i])
                    .newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
