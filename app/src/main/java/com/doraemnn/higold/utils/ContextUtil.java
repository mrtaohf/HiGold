package com.doraemnn.higold.utils;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.ArrayRes;

import com.doraemnn.higold.app.HiGoldApp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 获取资源值相关的工具类
 * Created by hongfei.tao on 2017/3/18.
 */
public class ContextUtil {

    public static Context getContext() {
        return HiGoldApp.getInstance().getApplicationContext();
    }

    public static Resources getResources() {
        return getContext().getResources();
    }

    public static List<String> getStringArray(@ArrayRes int resId) {
        List<String> list = null;

        if (resId > 0) {
            String[] strings = getResources().getStringArray(resId);
            list = new ArrayList<>();
            list.addAll(Arrays.asList(strings));
        }

        return list;
    }

}
