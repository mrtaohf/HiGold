package com.doraemnn.higold.utils;

import android.text.format.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * 时间转化工具
 */
@SuppressWarnings("all")
public class DateUtil {

    public static final String FORMAT1 = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String FORMAT2 = "yyyy-MM-dd";
    public static final String FORMAT3 = "yyyy";
    public static final String FORMAT4 = "yyyyMM";
    public static final String FORMAT5 = "MM-dd";
    public static final String FORMAT6 = "yyyy-MM";
    public static final String FORMAT7 = "yyyy年MM月dd日";
    public static final String FORMAT8 = "MM-dd HH:mm";
    public static final String FORMAT9 = "HH:mm";
    public static final String FORMAT10 = "yyyy-MM-dd HH:mm";
    public static final String FORMAT11 = "MM/dd";
    public static final String FORMAT12 = "MM";
    public static final String FORMAT13 = "yyyy.MM.dd";
    public static final String FORMAT14 = "yyyy/MM/dd";
    public static final String FORMAT15 = "MM-dd  HH:mm";
    public static final String FORMAT16 = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";


    private DateUtil() {
        //Utility Class
    }

    /**
     * 时间格式转化
     *
     * @param date     时间
     * @param fromType 转化格式
     * @param toType   目标格式
     * @return String
     */
    public static String format(String date, String fromType, String toType) {
        final SimpleDateFormat fromFormat = new SimpleDateFormat(fromType, Locale.CHINA);
        final SimpleDateFormat toFormat = new SimpleDateFormat(toType, Locale.CHINA);
        try {
            return toFormat.format(fromFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            return "parse error";
        }
    }

    /**
     * @param fromType 需要转化的类型
     * @return 时间戳
     */
    public static long formatDate(String source, String fromType) {
        try {
            DateFormat format = new SimpleDateFormat(fromType, Locale.CHINA);
            Date date = format.parse(source);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 优惠活动剩余时间
     *
     * @param arg 结束时间
     */
    public static String getRemainTime(String arg) {
        final long currentTime = DateUtil.formatDate(new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA).format(new Date
                ()), "yyyy-MM-dd");
        final long endTime = DateUtil.formatDate(arg, "yyyy-MM-dd");
        final long remainTime = endTime - currentTime;
        StringBuilder sb = new StringBuilder();
        if (remainTime < 0) {
            sb.append("该活动已过期");
        } else {
            sb.append("剩余");
            sb.append("<font color=#6adc37>");
            int count = (int) (1 + remainTime / (24 * 3600 * 1000));
            sb.append(count);
            sb.append("</font>天");
        }
        return sb.toString();
    }

    /**
     * 时间格式转化
     *
     * @param date   时间戳
     * @param toType 转化类型
     */
    public static String format(long date, String toType) {
        final SimpleDateFormat format = new SimpleDateFormat(toType, Locale.CHINA);
        return format.format(new Date(date));
    }

    /**
     * 时间显示规则[聊天\动态等]
     *
     * @param when 时间戳
     */
    public static String timeRule(long when) {
        if (DateUtils.isToday(when)) {//今天
            return format(when, FORMAT9);
        } else {
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(when);
            //显示时间
            int thenYear = calendar.get(Calendar.YEAR);
            int thenMonth = calendar.get(Calendar.MONTH);
            int thenMonthDay = calendar.get(Calendar.DATE);
            //当前时间
            calendar.setTimeInMillis(System.currentTimeMillis());
            if (thenYear == calendar.get(Calendar.YEAR) &&
                    thenMonth == calendar.get(Calendar.MONTH) &&
                    thenMonthDay == calendar.get(Calendar.DATE) - 1) {//昨天
                return String.format(Locale.CHINA, "昨天 %s", format(when, FORMAT9));
            } else if (thenYear == calendar.get(Calendar.YEAR) &&
                    thenMonth == calendar.get(Calendar.MONTH) &&
                    thenMonthDay == calendar.get(Calendar.DATE) - 2) {//前天
                return String.format(Locale.CHINA, "前天 %s", format(when, FORMAT9));
            } else if (thenYear == calendar.get(Calendar.YEAR)) {//今年的其他日期
                return format(when, FORMAT8);
            } else {//非今年的日期
                return format(when, FORMAT10);
            }
        }
    }

    /**
     * 成交时间
     *
     * @param month 往前推几个月
     */
    public static long dealTime(int month) {
        Calendar ca = Calendar.getInstance();
        Date date = new Date(System.currentTimeMillis());
        ca.setTime(date);
        ca.add(Calendar.MONTH, -month);
        return ca.getTimeInMillis() / 1000;
    }
}
