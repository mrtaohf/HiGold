package com.doraemnn.higold.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.doraemnn.higold.R;

/**
 * Glide工具类
 * Created by hongfei.tao on 2017/3/19.
 */
public class GlideUtil {

    public static void displayBitmap(Fragment fragment, ImageView imageView, String url) {

        Glide.with(fragment)
                .load(url)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .error(R.drawable.default_image)
                .centerCrop()
                .dontAnimate()
                .into(imageView);

    }


    public static void displayBitmap(Context context, ImageView imageView, Object url) {

        Glide.with(context)
                .load(url)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .error(R.drawable.default_image)
                .centerCrop()
                .dontAnimate()
                .into(imageView);

    }

}
