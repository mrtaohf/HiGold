package com.doraemnn.higold.utils;

import android.content.res.Resources;

/**
 * 尺寸计算相关工具类
 * Created by hongfei.tao on 2017/3/18.
 */
public class ScreenUtil {

    /**
     * 获取状态栏高度
     *
     * @return StatusHeight
     */
    public static int getStatusBarHeight() {
        Resources res = Resources.getSystem();
        int resId = res.getIdentifier("status_bar_height", "dimen", "android");
        if (resId > 0) {
            return res.getDimensionPixelSize(resId);
        }
        return 0;
    }

}
