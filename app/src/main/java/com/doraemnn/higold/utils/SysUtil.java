package com.doraemnn.higold.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;

import com.doraemnn.higold.app.HiGoldApp;

import java.util.Locale;

/**
 * <p>
 * 描述:SysUtil
 */
@SuppressWarnings("unused")
public final class SysUtil {

    private SysUtil() {
        //Utility Class
    }

    /**
     * 检查WIFI是否连接
     */
    public static boolean isWifiConnected() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) HiGoldApp.getInstance().getApplicationContext()
                        .getSystemService(
                                Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return wifiInfo != null;
    }

    /**
     * 检查手机网络(4G/3G/2G)是否连接
     */
    public static boolean isMobileNetworkConnected() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) HiGoldApp.getInstance().getApplicationContext()
                        .getSystemService(
                                Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobileNetworkInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        return mobileNetworkInfo != null;
    }

    /**
     * 检查是否有可用网络
     */
    public static boolean isNetworkConnected() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) HiGoldApp.getInstance().getApplicationContext()
                        .getSystemService(
                                Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null;
    }

    /**
     * 设备id
     */
    @SuppressWarnings("deprecation")
    public static String deviceId() {
        return String.format(Locale.CHINA, "35%d%d%d%d%d%d%d%d%d%d%d%d%d",
                Build.BOARD.length() % 10,
                Build.BRAND.length() % 10,
                Build.CPU_ABI.length() % 10,
                Build.DEVICE.length() % 10,
                Build.DISPLAY.length() % 10,
                Build.HOST.length() % 10,
                Build.ID.length() % 10,
                Build.MANUFACTURER.length() % 10,
                Build.MODEL.length() % 10,
                Build.PRODUCT.length() % 10,
                Build.TAGS.length() % 10,
                Build.TYPE.length() % 10,
                Build.USER.length() % 10);
    }

    /**
     * 当前进程名
     */
    public static String currentProcessName(Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager
                .getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }

    /**
     * 短信
     */
    public static void sendMsg(Context context, String mobile, String msg) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + mobile));
        intent.putExtra("sms_body", msg);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
