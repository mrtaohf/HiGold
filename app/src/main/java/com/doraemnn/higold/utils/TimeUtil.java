package com.doraemnn.higold.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * 日期时间工具类
 * Created by hongfei.tao on 2017/3/19.
 */
public class TimeUtil {

    public static int compareDate(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINESE);
        try {
            Date date = sdf.parse(time);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            int result = calendar.compareTo(Calendar.getInstance());
            if (result < 0)
                return -1;
            else if (result > 0)
                return 1;
            else
                return result;

        } catch (ParseException e) {
            return -2;
        }
    }

    public static void formatDate(String time) {

    }

}
