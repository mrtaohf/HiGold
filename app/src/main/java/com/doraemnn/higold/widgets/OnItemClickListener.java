package com.doraemnn.higold.widgets;

/**
 * 条目点击回调监听器
 * Created by hongfei.tao on 2017/3/19.
 */
public interface OnItemClickListener {

    void onItemClick();

}
