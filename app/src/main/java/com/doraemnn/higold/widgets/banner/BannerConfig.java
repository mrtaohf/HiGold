package com.doraemnn.higold.widgets.banner;

/**
 * Created by leonwang on 2017/1/10.
 */

public class BannerConfig {

    /**
     * banner
     */
    public static final int PADDING_SIZE=5;
    public static final int TIME=2000;
    public static final int DURATION=800;
    public static final boolean IS_AUTO_PLAY=true;
    public static final boolean IS_SCROLL=true;

}
